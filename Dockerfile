FROM golang
RUN mkdir -p /adik/src/snippet/
WORKDIR /adik/src/snippet/
COPY . /adik/src/snippet/
CMD ["go", "run","./cmd/web"]