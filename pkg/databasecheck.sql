CREATE TABLE IF NOT EXISTS  snippets (
    id SERIAL PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    content TEXT NOT NULL,
    created timestamp NOT NULL,
    expires timestamp NOT NULL
);

CREATE INDEX idx_snippets_created ON snippets(created);